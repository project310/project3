package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.widget.TextView;
import android.widget.Button;
import android.view.View;
import android.os.Bundle;
import android.widget.EditText;
import android.content.Intent;

public class MainActivity<Resorces> extends AppCompatActivity {

    Button first;
    TextView Text, Warning;
    EditText login, pass;
    int k;
    String[] PASS, LOG;
    public final static String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    Intent intent;
    int TextColor;
    int BackColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextColor = ContextCompat.getColor(this, R.color.red);
        BackColor = ContextCompat.getColor(this, R.color.lightred);
        PASS = getResources().getStringArray(R.array.Pass);
        LOG = getResources().getStringArray(R.array.Log);
        Text = (TextView) findViewById(R.id.Text);
        Warning = (TextView) findViewById(R.id.WAR);
        first = (Button) findViewById(R.id.first);
        login = (EditText) findViewById(R.id.login);
        pass = (EditText) findViewById(R.id.pass);
        intent = new Intent(this, ActivityTwo.class);

        setTitle("КТбо1-6 МоскаленкоАС");
        first.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String log = login.getText().toString();
                String pas = pass.getText().toString();

                intent.putExtra("login", log);
                k = 1;
                for(int i = 0;i<90 && k == 1;i++)
                {
                    if(log.equals(LOG[i]) && pas.equals(PASS[i]))
                    {
                        startActivity(intent);
                        k = 0;
                    }
                }
                if(k==1){
                    Text.setText("NO");
                    Warning.setText("Неправильный пароль или логин!!! Попробуйте ещё раз!!!");
                    Warning.setBackgroundColor(BackColor);
                    Warning.setTextColor(TextColor);
                }
            }

        });
    }
}
