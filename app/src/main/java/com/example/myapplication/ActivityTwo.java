package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;
import android.os.Bundle;
import android.content.Intent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ActivityTwo extends AppCompatActivity {

    WebView Web;
    String  log;
    int k=1;
    int count = 0;
    String[] LOG;
    String[] PAGE;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two2);
        Web = findViewById(R.id.web);
        LOG = getResources().getStringArray(R.array.Log);
        PAGE = getResources().getStringArray(R.array.Page);
        log = getIntent().getStringExtra("login");
        setTitle(log);
        for (int i = 0; i<90 && k==1;i++)
        {
            if(log.equals(LOG[i]))
            {
                k = 0;
                count = i;
            }
        }
        Web.loadUrl("https://" + PAGE[count]);
    }
}
